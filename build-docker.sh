#!/bin/bash

DOCKER_IMAGE_TAG=${1:-latest}
DOCKER_IMAGE_REGISTRY=${2:-$(basename "$(pwd)")}

./build-doc-docker.sh

docker run --rm \
    --user $(id -u):$(id -g) \
    --mount type=bind,source="$(pwd)",destination=/build \
    --workdir /build \
    registry.gitlab.com/digimountain/docker/angular-cli:8.2.1 \
    /bin/bash -c \
    "npm ci && ng build --prod"

docker build --force-rm -t $DOCKER_IMAGE_REGISTRY:$DOCKER_IMAGE_TAG $(pwd)
