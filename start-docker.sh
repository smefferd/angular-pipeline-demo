DOCKER_IMAGE_TAG=${1:-latest}
DOCKER_IMAGE_REGISTRY=${2:-$(basename "$(pwd)")}

docker run --rm -d -p 5000:5000 \
    --name $(basename $DOCKER_IMAGE_REGISTRY) \
    $DOCKER_IMAGE_REGISTRY:$DOCKER_IMAGE_TAG 

echo
echo Application demo url is http://localhost:5000
