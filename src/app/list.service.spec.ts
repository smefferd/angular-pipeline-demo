import { TestBed } from '@angular/core/testing';

import { ListService } from './list.service';
import { ListItem } from './listitem';

describe('ListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListService = TestBed.get(ListService);
    expect(service).toBeTruthy();
  });

  it('should save list items with the addItem method', (done: DoneFn) => {
    const service: ListService = TestBed.get(ListService);
    const item1: ListItem = new ListItem();
    item1.itemText = 'test1';
	service.addListItem(item1);
    let foundListItems: ListItem[];
    service.getListItems().subscribe(listItems => {
            foundListItems = listItems;
            expect(listItems.pop().itemText).toEqual('test1');
            done();
	});
  });

  it('should remove list items with the removeItem method', (done: DoneFn) => {
    const service: ListService = TestBed.get(ListService);
    const item1: ListItem = new ListItem();
    item1.itemText = 'test1';
	service.addListItem(item1);
    const item2: ListItem = new ListItem();
    item2.itemText = 'test2';
    service.addListItem(item2);
    let foundListItems: ListItem[];
    service.getListItems().subscribe(listItems => {
            foundListItems = listItems;
            expect(listItems.length).toEqual(2);
            done();
	});
    service.removeListItem(item1).subscribe(listItems => {
            foundListItems = listItems;
            expect(listItems.length).toEqual(1);
            done();
	});
  });

  it('should reorder list items down 1 with the adjustOrdering method', (done: DoneFn) => {
    const service: ListService = TestBed.get(ListService);
    const item1: ListItem = new ListItem();
    item1.itemText = 'test1';
	service.addListItem(item1);
    const item2: ListItem = new ListItem();
    item2.itemText = 'test2';
    service.addListItem(item2);
    const item3: ListItem = new ListItem();
    item3.itemText = 'test3';
    service.addListItem(item3);
    let foundListItems: ListItem[];
    service.adjustOrdering(item1,2);
    service.getListItems().subscribe(listItems => {
            foundListItems = listItems;
            expect(listItems[0].itemText).toEqual('test2');
            expect(listItems[0].ordering).toEqual(1);
            expect(listItems[1].itemText).toEqual('test1');
            expect(listItems[1].ordering).toEqual(2);
            expect(listItems[2].itemText).toEqual('test3');
            expect(listItems[2].ordering).toEqual(3);
            done();
	});
  });

  it('should reorder list items up 1 with the adjustOrdering method', (done: DoneFn) => {
    const service: ListService = TestBed.get(ListService);
    const item1: ListItem = new ListItem();
    item1.itemText = 'test1';
	service.addListItem(item1);
    const item2: ListItem = new ListItem();
    item2.itemText = 'test2';
    service.addListItem(item2);
    const item3: ListItem = new ListItem();
    item3.itemText = 'test3';
    service.addListItem(item3);
    let foundListItems: ListItem[];
    service.adjustOrdering(item3,2);
    service.getListItems().subscribe(listItems => {
            foundListItems = listItems;
            expect(listItems[0].itemText).toEqual('test1');
            expect(listItems[0].ordering).toEqual(1);
            expect(listItems[1].itemText).toEqual('test3');
            expect(listItems[1].ordering).toEqual(2);
            expect(listItems[2].itemText).toEqual('test2');
            expect(listItems[2].ordering).toEqual(3);
            done();
	});
  });

  it('should not move items off the top with adjustOrdering', (done: DoneFn) => {
    const service: ListService = TestBed.get(ListService);
    const item1: ListItem = new ListItem();
    item1.itemText = 'test1';
	service.addListItem(item1);
    const item2: ListItem = new ListItem();
    item2.itemText = 'test2';
    service.addListItem(item2);
    let foundListItems: ListItem[];
    service.adjustOrdering(item2,1);
    service.getListItems().subscribe(listItems => {
            foundListItems = listItems;
            expect(listItems.length).toEqual(2);
            expect(listItems[0].itemText).toEqual('test2');
            expect(listItems[0].ordering).toEqual(1);
            expect(listItems[1].itemText).toEqual('test1');
            expect(listItems[1].ordering).toEqual(2);
            done();
	});
  });

  it('should not move items off the bottom with adjustOrdering', (done: DoneFn) => {
    const service: ListService = TestBed.get(ListService);
    const item1: ListItem = new ListItem();
    item1.itemText = 'test1';
	service.addListItem(item1);
    const item2: ListItem = new ListItem();
    item2.itemText = 'test2';
    service.addListItem(item2);
    let foundListItems: ListItem[];
    service.adjustOrdering(item1,2);
    service.getListItems().subscribe(listItems => {
            foundListItems = listItems;
            expect(listItems.length).toEqual(2);
            expect(listItems[0].itemText).toEqual('test2');
            expect(listItems[0].ordering).toEqual(1);
            expect(listItems[1].itemText).toEqual('test1');
            expect(listItems[1].ordering).toEqual(2);
            done();
	});
  });

  it('should move up one place with the moveUpListItem method', (done: DoneFn) => {
    const service: ListService = TestBed.get(ListService);
    const item1: ListItem = new ListItem();
    item1.itemText = 'test1';
	service.addListItem(item1);
    const item2: ListItem = new ListItem();
    item2.itemText = 'test2';
    service.addListItem(item2);
    const item3: ListItem = new ListItem();
    item3.itemText = 'test3';
    service.addListItem(item3);
    let foundListItems: ListItem[];
    service.adjustOrdering(item3,2);
    service.getListItems().subscribe(listItems => {
            foundListItems = listItems;
            expect(listItems[0].itemText).toEqual('test1');
            expect(listItems[0].ordering).toEqual(1);
            expect(listItems[1].itemText).toEqual('test3');
            expect(listItems[1].ordering).toEqual(2);
            expect(listItems[2].itemText).toEqual('test2');
            expect(listItems[2].ordering).toEqual(3);
            done();
	});
  });

  it('should move down one place with the moveDownListItem method', (done: DoneFn) => {
    const service: ListService = TestBed.get(ListService);
    const item1: ListItem = new ListItem();
    item1.itemText = 'test1';
	service.addListItem(item1);
    const item2: ListItem = new ListItem();
    item2.itemText = 'test2';
    service.addListItem(item2);
    const item3: ListItem = new ListItem();
    item3.itemText = 'test3';
    service.addListItem(item3);
    let foundListItems: ListItem[];
    service.adjustOrdering(item1,2);
    service.getListItems().subscribe(listItems => {
            foundListItems = listItems;
            expect(listItems[0].itemText).toEqual('test2');
            expect(listItems[0].ordering).toEqual(1);
            expect(listItems[1].itemText).toEqual('test1');
            expect(listItems[1].ordering).toEqual(2);
            expect(listItems[2].itemText).toEqual('test3');
            expect(listItems[2].ordering).toEqual(3);
            done();
	});
  });
});
