import { Injectable } from '@angular/core';
import { ListItem } from './listitem';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  listItems: ListItem[] = [];

  constructor() { }

  getListItems(): Observable<ListItem[]> {
    return of(Object.create(this.listItems));
  }

  addListItem(listItem: ListItem): Observable<ListItem[]> {
    listItem.id = this.listItems.length > 0 ? Math.max(...this.listItems.map(li => li.id)) + 1 : 1;
    listItem.ordering = this.listItems.length + 1;
    this.listItems.push(listItem);
    return of(Object.create(this.listItems));
  }

  sortListItems(listItems: ListItem[]): ListItem[] {
    return listItems.sort((n1,n2) => {
      if (n1.ordering > n2.ordering) return 1;
      if (n1.ordering < n2.ordering) return -1;
      return 0;
    });
  }

  adjustOrdering(reorderedListItem: ListItem, newOrdering: number) {
    var adjustedList: ListItem[] = [];
    var ordering = 1;
    for (let listItem of this.sortListItems(this.listItems)) {
      if (listItem.id == reorderedListItem.id) {
        continue;
      }
      if (ordering == newOrdering) {
        reorderedListItem.ordering = ordering;
        adjustedList.push(reorderedListItem);
        ordering++;
      }
      listItem.ordering = ordering;
      adjustedList.push(listItem);
      ordering++;
      if (ordering == newOrdering) {
	    reorderedListItem.ordering = ordering;
        adjustedList.push(reorderedListItem);
        ordering++;
      }
    }
    this.listItems = adjustedList;
  }

  removeListItem(listItemToRemove: ListItem): Observable<ListItem[]> {
    this.adjustOrdering(listItemToRemove, 0);
    return of(Object.create(this.listItems));
  }

  moveUpListItem(listItem: ListItem): Observable<ListItem[]> {
    if (listItem.ordering > 1) {
       this.adjustOrdering(listItem, (listItem.ordering - 1));
    }
    return of(Object.create(this.listItems));
  }

  moveDownListItem(listItem: ListItem): Observable<ListItem[]> {
    if (listItem.ordering < this.listItems.length) {
      this.adjustOrdering(listItem, (listItem.ordering + 1));
    }
    return of(Object.create(this.listItems));
  }
}
