export class ListItem {
  id: number;
  ordering: number;
  itemText: string;
}
