import { Component, OnInit } from '@angular/core';
import { ListItem } from '../listitem';
import { ListService } from '../list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  listItems: ListItem[];
  newItem: ListItem = new ListItem();

  constructor(private listService: ListService) { }

  ngOnInit() {
    this.getListItems();		 
  }

  getListItems() {
    this.listService.getListItems()
           .subscribe(listItems => this.listItems = listItems);
  }

  addItem() {
    this.listService.addListItem(this.newItem)
           .subscribe(listItems => this.listItems = listItems); 
    this.newItem = new ListItem();
  }

  removeListItem(listItemToRemove: ListItem) {
    this.listService.removeListItem(listItemToRemove)
           .subscribe(listItems => this.listItems = listItems);
  }

  moveUpListItem(listItem: ListItem) {
    this.listService.moveUpListItem(listItem)
           .subscribe(listItems => this.listItems = listItems);
  }

  moveDownListItem(listItem: ListItem) {
    this.listService.moveDownListItem(listItem)
           .subscribe(listItems => this.listItems = listItems);
  }
}
