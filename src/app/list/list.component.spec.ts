import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListComponent } from './list.component';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ListItem } from '../listitem';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [ ListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    component.listItems = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a list item with the add button', () => {
    const listField: HTMLInputElement = fixture.debugElement.query(By.css('input')).nativeElement;
    listField.value = 'test1';
    listField.dispatchEvent(new Event('input'));
    fixture.debugElement.query(By.css('.add-item-button')).triggerEventHandler('click', null);
    expect(component.listItems.pop().itemText).toEqual('test1');
  });

  it('should add a list item with the enter key', () => {
    const listField: HTMLInputElement = fixture.debugElement.query(By.css('input')).nativeElement;
    listField.value = 'test2';
    listField.dispatchEvent(new Event('input'));
    listField.dispatchEvent(new KeyboardEvent('keyup', { key: 'Enter' }));    
    expect(component.listItems.pop().itemText).toEqual('test2');
  });

  it('should remove the list item when the remove button is clicked', () => {
    component.newItem = new ListItem();
    component.newItem.itemText = 'test1';
    component.addItem();
    component.newItem = new ListItem();
    component.newItem.itemText = 'test2';
    component.addItem();
    fixture.detectChanges();
    const value = fixture.debugElement.query(By.css('.list-item'));
	value.query(By.css('.remove-item-button')).triggerEventHandler('click', null);
    component.getListItems();
    expect(component.listItems.length).toEqual(1);
    expect(component.listItems.pop().itemText).toEqual('test2');
  });

  it('should move the list item down when down button is clicked', () => {
    // Put 3 items on the list
    component.newItem = new ListItem();
    component.newItem.itemText = 'test1';
    component.addItem();
    component.newItem = new ListItem();
    component.newItem.itemText = 'test2';
    component.addItem();
    component.newItem = new ListItem();
    component.newItem.itemText = 'test3';
    component.addItem();
    fixture.detectChanges();
    // Find the first one in the view and click the down button
    const beforeItemDivs = fixture.debugElement.queryAll(By.css('.list-item'));
    const orderingBefore = beforeItemDivs[0].query(By.css('.list-item-ordering'))
          .nativeElement.innerText;
    const textBefore = beforeItemDivs[0].query(By.css('.list-item-text'))
          .nativeElement.innerText;
    expect(orderingBefore).toEqual('1');
    expect(textBefore).toEqual('test1');
    beforeItemDivs[0].query(By.css('.down-item-button')).triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.listItems.length).toEqual(3);
    // Look for it in the second list item position
    // since it should have moved down
    const afterItemDivs = fixture.debugElement.queryAll(By.css('.list-item'));
    const orderingAfter = afterItemDivs[1].query(By.css('.list-item-ordering'))
          .nativeElement.innerText;
    const textAfter = afterItemDivs[1].query(By.css('.list-item-text'))
          .nativeElement.innerText;
    expect(orderingAfter).toEqual('2');
    expect(textAfter).toEqual('test1');
  });

  it('should not move the list item off the list if it becomes the last one', () => {
    // Put 2 items on the list
    component.newItem = new ListItem();
    component.newItem.itemText = 'test1';
    component.addItem();
    component.newItem = new ListItem();
    component.newItem.itemText = 'test2';
    component.addItem();
    fixture.detectChanges();
    // Find the first one in the view and click the down button
    const beforeItemDivs = fixture.debugElement.queryAll(By.css('.list-item'));
    const orderingBefore = beforeItemDivs[0].query(By.css('.list-item-ordering'))
          .nativeElement.innerText;
    const textBefore = beforeItemDivs[0].query(By.css('.list-item-text'))
          .nativeElement.innerText;
    expect(orderingBefore).toEqual('1');
    expect(textBefore).toEqual('test1');
    beforeItemDivs[0].query(By.css('.down-item-button')).triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.listItems.length).toEqual(2);
  });

  it('should move the list item up when up button is clicked', () => {
    // Put 2 items on the list
    component.newItem = new ListItem();
    component.newItem.itemText = 'test1';
    component.addItem();
    component.newItem = new ListItem();
    component.newItem.itemText = 'test2';
    component.addItem();
    fixture.detectChanges();
    // Find the second one in the view and click the up button
    const listItemDivs = fixture.debugElement.queryAll(By.css('.list-item'));
    const orderingBefore = listItemDivs[1].query(By.css('.list-item-ordering'))
          .nativeElement.innerText;
    const textBefore = listItemDivs[1].query(By.css('.list-item-text'))
          .nativeElement.innerText;
    expect(orderingBefore).toEqual('2');
    expect(textBefore).toEqual('test2');
    listItemDivs[1].query(By.css('.up-item-button')).triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.listItems.length).toEqual(2);
    // Look for it in the first position now
    const afterItemDivs = fixture.debugElement.queryAll(By.css('.list-item'));
    const orderingAfter = afterItemDivs[0].query(By.css('.list-item-ordering'))
          .nativeElement.innerText;
    const textAfter = afterItemDivs[0].query(By.css('.list-item-text'))
          .nativeElement.innerText;
    expect(orderingAfter).toEqual('1');
    expect(textAfter).toEqual('test2');
  });

  it('should not let a list item move off the top of the list', () => {
    // Put 2 items on the list
    component.newItem = new ListItem();
    component.newItem.itemText = 'test1';
    component.addItem();
    component.newItem = new ListItem();
    component.newItem.itemText = 'test2';
    component.addItem();
    fixture.detectChanges();
    // Find the first one in the view and click the up button
    const firstItemDiv = fixture.debugElement.query(By.css('.list-item'));
    const orderingBefore = firstItemDiv.query(By.css('.list-item-ordering'))
          .nativeElement.innerText;
    const textBefore = firstItemDiv.query(By.css('.list-item-text'))
          .nativeElement.innerText;
    expect(orderingBefore).toEqual('1');
    expect(textBefore).toEqual('test1');
    firstItemDiv.query(By.css('.up-item-button')).triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.listItems.length).toEqual(2);
    // Expect it to still be in the first list item position
    const afterItemDiv = fixture.debugElement.query(By.css('.list-item'));
    const orderingAfter = afterItemDiv.query(By.css('.list-item-ordering'))
          .nativeElement.innerText;
    const textAfter = afterItemDiv.query(By.css('.list-item-text'))
          .nativeElement.innerText;
    expect(orderingAfter).toEqual('1');
    expect(textAfter).toEqual('test1');
  });

  it('should not let a list item move off the bottom of the list', () => {
    // Put 2 items on the list
    component.newItem = new ListItem();
    component.newItem.itemText = 'test1';
    component.addItem();
    component.newItem = new ListItem();
    component.newItem.itemText = 'test2';
    component.addItem();
    fixture.detectChanges();
    // Find the second one in the view and click the down button
    const listItemDivs = fixture.debugElement.queryAll(By.css('.list-item'));
    const orderingBefore = listItemDivs[1].query(By.css('.list-item-ordering'))
          .nativeElement.innerText;
    const textBefore = listItemDivs[1].query(By.css('.list-item-text'))
          .nativeElement.innerText;
    expect(orderingBefore).toEqual('2');
    expect(textBefore).toEqual('test2');
    listItemDivs[1].query(By.css('.down-item-button')).triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.listItems.length).toEqual(2);
    // Expect it to still be the second list item
    const afterItemDivs = fixture.debugElement.queryAll(By.css('.list-item'));
    const orderingAfter = afterItemDivs[1].query(By.css('.list-item-ordering'))
          .nativeElement.innerText;
    const textAfter = afterItemDivs[1].query(By.css('.list-item-text'))
          .nativeElement.innerText;
    expect(orderingAfter).toEqual('2');
    expect(textAfter).toEqual('test2');
  });
});
