#!/bin/bash

./build-doc-docker.sh

docker run --rm \
    --user $(id -u):$(id -g) \
    --mount type=bind,source="$(pwd)",destination=/build \
    --workdir /build \
    registry.gitlab.com/digimountain/docker/angular-cli:8.2.1 \
    /bin/bash -c \
    "npm ci && ng test --watch=false --progress=false --code-coverage"
