#!/bin/bash

docker run --rm \
    --user $(id -u):$(id -g) \
    --mount type=bind,source="$(pwd)/src/app",destination=/documents \
    asciidoctor/docker-asciidoctor \
    asciidoctor -s -v -o app.component.html doc/doc.adoc
