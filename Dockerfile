FROM nginx
EXPOSE 5000
COPY dist/listapp/ /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/conf.d/default.conf
CMD nginx -g 'daemon off;'
